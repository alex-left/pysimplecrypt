#!/usr/bin/python3

import subprocess
import sys
import shutil
from pathlib import Path


def main():
    gpg2_path = shutil.which("gpg2")
    paths = [Path(p) for p in sys.argv[1:]]

    for path in paths:
        if path.is_file():
            filename = path.resolve()
            try:
                proc = subprocess.run([gpg2_path, "--symmetric", 
                                        "--cipher-algo", "AES256", str(filename)])
            except Exception as e:
                print("Failed:  {0}\n {}".format(str(filename), e))
            if proc.returncode == 0:
                shutil.os.remove(str(filename))
        else:
            print("'{0}' is not a valid file".format(str(path)))

if __name__ == "__main__":
    main()

