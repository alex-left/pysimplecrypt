#!/usr/bin/python3

import subprocess
import sys
import shutil
from pathlib import Path


#gpg2 --symmetric --cipher-algo AES256 test.txt
# gpg2 --output test.txt --decrypt test.txt.gpg
def main():
    gpg2_path = shutil.which("gpg2")
    paths = [Path(p) for p in sys.argv[1:]]

    for path in paths:
        if path.is_file():
            parent = path.parent.resolve()
            filename = path.resolve()
            try:
                proc = subprocess.run([gpg2_path, "--output", str(parent / path.stem), 
                                       "--decrypt", str(filename)])
            except Exception as e:
                print("Failed:  {0}\n {1}".format(str(filename), e))
            if proc.returncode == 0:
                shutil.os.remove(str(filename))
        else:
            print("'{0}' is not a valid file".format(str(path)))

if __name__ == "__main__":
    main()

